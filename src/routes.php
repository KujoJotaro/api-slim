<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\Connection;

//Agregar Cors a todas las request

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});
//Rutas de Books

//Listar
$app->get('/book', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('books')->select('*')->get();
    return $response->withJson($rows);
});
//Buscar por Id
$app->get('/book/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('books')->select('*')->where('id', '=', $args['id'])->first();
    return $response->withJson($rows);
});

//Actualizar
$app->put('/book/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $db->table('books')
        ->where(['id' => $args['id']])
        ->update(
            [
                'tittle' => $body['title'],
                'subtittle' => $body['subtittle'],
                'rate' => $body['rate'],
                'notes' => $body['notes'],
                'price' => $body['price'],
                'cover' => $body['cover'],
                'url' => $body['url'],
                'readed' => $body['readed'],
                'have' => $body['have'],
                'author_id' => $body['author_id'],
                'type_id' => $body['type_id']
            ]);
    return $response->withStatus(200);
});


//Crear
$app->post('/book', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $rows = $db->table('books')
        ->insert(
            [
                'tittle' => $body['title'],
                'subtittle' => $body['subtittle'],
                'rate' => $body['rate'],
                'notes' => $body['notes'],
                'price' => $body['price'],
                'cover' => $body['cover'],
                'url' => $body['url'],
                'readed' => $body['readed'],
                'have' => $body['have'],
                'author_id' => $body['author_id'],
                'type_id' => $body['type_id']
                ]);
    return $response->withStatus(200);
});

//Borrar
$app->delete('/book/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('books')->delete($args['id']);
    return $response->withJson($rows);
});


//Rutas de Book types

$app->get('/type', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('types')->select('*')->get();
    return $response->withJson($rows);
});

$app->get('/type/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $type = $db->table('types')->select('*')->where('id', '=', $args['id'])->first();
    return $response->withJson($type);
});

$app->put('/type/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $db->table('types')->where(['id' => $args['id']])->update([ 'name' => $body['name'], 'description' => $body['description']]);
    return $response->withStatus(200);
});

$app->post('/type', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $db->table('types')->insert(['name' => $body['name'], 'description' => $body['description']]);
    return $response->withStatus(200);

});

$app->delete('/type/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $success = $db->table('types')->delete($args['id']);

    return $response->withJson($success);
});



//Rutas de Author

$app->get('/author', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('authors')->select('*')->get();
    return $response->withJson($rows);
});

$app->get('/author/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('authors')->select('*')->where('id', '=', $args['id'])->first();
    return $response->withJson($rows);
});

$app->put('/author/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $db->table('authors')->where(['id' => $args['id']])->update(['name' => $body['name']]);
    return $response->withStatus(200);
});

$app->post('/author/', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $body = $request->getParsedBody();
    $db->table('authors')->insert(['name' => $body['name']]);
    return $response->withStatus(200);
});

$app->delete('/author/{id}', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $db->table('authors')->delete($args['id']);
    return $response->withStatus(200);
});


//Ruta de User

$app->get('/user', function (Request $request, Response $response, array $args) {
    $db = $this->get('db');
    $rows = $db->table('users')->select('*')->get();
    return $response->withJson($rows);
});


