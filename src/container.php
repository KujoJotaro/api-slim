<?php
/**
 * Created by PhpStorm.
 * User: Omar
 * Date: 28/07/2018
 * Time: 2:59
 */

use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\Connection;

$container['db'] = function (Container $container) {
    $settings = $container->get('settings');

    $config = [
        'driver' => 'mysql',
        'host' => $settings['db']['host'],
        'database' => $settings['db']['database'],
        'username' => $settings['db']['username'],
        'password' => $settings['db']['password'],
        'charset'  => $settings['db']['charset'],
        'collation' => $settings['db']['collation'],
    ];

    $factory = new ConnectionFactory(new \Illuminate\Container\Container());

    return $factory->make($config);
};

$container['pdo'] = function (Container $container) {
    return $container->get('db')->getPdo();
};